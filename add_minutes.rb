# call-seq:
# 	add_minutes( time=String, minutes=Integer )
#  
#	Adds the number of minutes to time. Expects time to match [H]H:MM format.
# Will throw ArgumentError if given invalid params
def add_minutes( initial_time, minutes )

	# verify initial_time has the correct format
	raise ArgumentError, "Invalid time format, expecting String" if initial_time.class != String
	raise ArgumentError, "Invalid time format, expecting [H]H:MM" unless initial_time.match(/\A\d{1,2}:\d{2} (AM|PM)\z/)

	# verify 
	raise ArgumentError, "Invalid minutes format, expecting Integer" if minutes.class != Fixnum 

	(hr, min, period) = initial_time.gsub(/\s/, ':').split(':')
	hr = hr.to_i  
	min = min.to_i
	
	# convert to military time
	if hr == 12
		if period == "AM"
			hr = 0 
		else
			hr = 12
		end
	else
		hr += 12 if period == "PM" 
	end

	# add minutes to time
	min += minutes

	# update hour
	hr += min / 60
	# ensure hour is within 0-23
	hr -= 24 if hr > 23
	hr += 24 if hr < 0

	# update period
	period = ( hr < 12 ) ? "AM" : "PM"

	# undo military time
	hr = hr % 12
	hr = 12 if hr == 0

	# update minutes to be <60
	min = min % 60 

	return "#{hr}:#{sprintf('%02d', min)} #{period}"
end
