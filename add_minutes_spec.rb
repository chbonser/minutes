$LOAD_PATH.unshift(File.dirname(__FILE__))
require 'add_minutes'

describe "add_minutes" do 
  it "throws an error when time stamp is not a string" do
		expect { add_minutes(9, 10) }.to raise_error( ArgumentError )
	end
  it "throws an error when time stamp includes seconds" do
		expect { add_minutes("9:23.2 AM", 10) }.to raise_error( ArgumentError )
	end
  it "throws an error when time stamp does not include AM or PM" do
		expect { add_minutes("9:23", 10) }.to raise_error( ArgumentError )
	end
  it "throws an error when time stamp does not include AM or PM" do
		expect { add_minutes("9:23 AM", 10.2) }.to raise_error( ArgumentError )
	end
  it "correctly adds minutes for simple cases" do
		add_minutes("9:23 AM", 10).should eq("9:33 AM")
	end
  it "correctly adds minutes when crossing hour boundaries" do
		add_minutes("9:23 AM", 40).should eq("10:03 AM")
	end
  it "correctly toggles AM/PM at 12:00PM boundary" do
		add_minutes("11:23 AM", 40).should eq("12:03 PM")
		add_minutes("12:03 PM", -40).should eq("11:23 AM")
	end
  it "correctly toggles AM/PM at 12:00AM boundary" do
		add_minutes("11:23 PM", 40).should eq("12:03 AM")
		add_minutes("12:03 AM", -40).should eq("11:23 PM")
	end
end
